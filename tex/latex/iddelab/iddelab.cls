%% ============================================================
%% LaTeX osztály az IDDE laborkomponenseire
%% Szerző: Sulyok Csaba <csaba.sulyok@gmail.com>
%% ============================================================

\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{iddelab}[2019/08/29 IDDE labor]


% kiindulópont: article.cls
\LoadClass[a4paper,10pt,oneside]{article}


% magyarosító
\RequirePackage[utf8]{inputenc}
\RequirePackage{t1enc}
\RequirePackage[magyar]{babel}


% magyar indentálás
\RequirePackage{indentfirst}


% betűtípusok
\RequirePackage{fontspec}
\setmainfont[Ligatures=TeX, 
             Extension=.ttf,
             UprightFont=*-Regular,
             ItalicFont=*-Italic,
             BoldFont=*-Bold,
             BoldItalicFont=*-BoldItalic]{OpenSans}
\setmonofont[Ligatures=NoCommon, 
             Extension=.ttf]{Consolas}


% margó és sortávolság beállítása
\RequirePackage{geometry}
\RequirePackage{setspace}
\geometry{a4paper, left=15mm, right=15mm, top=20mm, bottom=20mm}
\linespread{1.2}


% set text colors
\RequirePackage{xcolor}
\definecolor{LightBlue}{HTML}{f0fbff}
\definecolor{BrightBlue}{HTML}{226291}
\definecolor{DarkBlue}{HTML}{071e2e}
\definecolor{Red}{HTML}{e95429}
\definecolor{Blue}{HTML}{016678}
\definecolor{Cyan}{HTML}{1da6b9}
\definecolor{Orange}{HTML}{fd8f1f}
\definecolor{LightBrown}{HTML}{fffaf2}


% címlap
\RequirePackage{textpos}
\RequirePackage{eso-pic}
\RequirePackage{tcolorbox}
\makeatletter
\def\@maketitle{%
    \null
    \vskip 5cm \par
    \begin{tcolorbox}[colback=white, colframe=DarkBlue, opacityback=0.35, opacityframe=0.35, left skip=-3cm, right skip=-3cm]%
        {\LARGE \bfseries \hspace{2cm} \@title \par}
    \end{tcolorbox}
    \vskip 3cm}
\makeatother


% \texttt színek
\let\Oldtexttt\texttt
\renewcommand\texttt[1]{{\ttfamily\color{Blue}#1}}


% url stílusok
\RequirePackage{hyperref}
\hypersetup{
  colorlinks=true,
  linkcolor=Blue,
  urlcolor=Blue
}


% ne számozzuk a fejezeteket
\RequirePackage{titlesec}
\titleformat{\section}{\normalfont\Large\bfseries}{}{0em}{}{}
\titleformat{\subsection}{\normalfont\large\bfseries}{}{1em}{\titlerule[0.8pt]\\}[]
\titleformat{\subsubsection}{\normalfont\normalsize\bfseries\itshape}{}{2em}{}[]


% oldalfejlécek tartalmazzák a címet és oldalszámot
\RequirePackage{fancyhdr}
\pagestyle{fancy}
\lhead{\itshape\MakeUppercase\headertitle}
\rhead{\thepage}
\cfoot{}


% kisebb itemize/enumerate skippek
\let\tempone\itemize
\let\temptwo\enditemize
\renewenvironment{itemize}{\tempone\itemsep0em\vspace{-0.5em}}{\temptwo}


% env amellyel boxot használhatunk
\newenvironment{exercise}[1]%
    {\begin{tcolorbox}[colback=LightBlue, colframe=DarkBlue, left skip=1.2cm, title=#1, fonttitle=\bfseries\itshape]}%
    {\end{tcolorbox}}


% bugfix
\makeatletter
\global\let\tikz@ensure@dollar@catcode=\relax
\makeatother